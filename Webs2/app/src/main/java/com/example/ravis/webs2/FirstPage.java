package com.example.ravis.webs2;
import com.example.ravis.webs2.DatabaseHandle.*;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.ravis.webs2.DatabaseHandle.DatabaseCreator;
import com.example.ravis.webs2.DatabaseHandle.getGoals;

import java.util.ArrayList;

public class FirstPage extends AppCompatActivity {

    RecyclerView firstpageRview;
    static Context FirstpageContext;
    RecyclerView.Adapter firstpageRviewAdapter;
    TextView setupfunding, setupgoalsaccount;
    Context firstpagecontext;
    RecyclerView.LayoutManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_page);
        firstpagecontext=this;
        FirstpageContext=this;
        GoalsPojo pojo=new GoalsPojo();
        SQLiteDatabase db= new DatabaseCreator(this).getReadableDatabase();
        firstpageRview=(RecyclerView)findViewById(R.id.firstpageRview);
        manager=new LinearLayoutManager(this);
        firstpagecontext = this;
        firstpageRview = (RecyclerView) findViewById(R.id.firstpageRview);
        manager = new LinearLayoutManager(this);
        firstpageRview.setNestedScrollingEnabled(false);
        firstpageRviewAdapter = new firstpageRviewAdapter(this, pojo);
        firstpageRview.setAdapter(firstpageRviewAdapter);
        firstpageRview.setLayoutManager(manager);
        setupfunding=(TextView)findViewById(R.id.setupfunding);
        setupgoalsaccount=(TextView)findViewById(R.id.setupgoalsAcccount);
setupgoalsaccount.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent in=new Intent(FirstPage.this,Setup.class);
        finish();
        startActivity(in);
    }
});
        setupfunding = (TextView) findViewById(R.id.setupfunding);
        setupgoalsaccount = (TextView) findViewById(R.id.setupgoalsAcccount);
        setupgoalsaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(FirstPage.this, Setup.class);
                finish();
                startActivity(in);
            }
        });
        //starting new activity finishing this
        setupfunding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(FirstPage.this, Setup.class);
                finish();
                startActivity(in);
            }
        });
    }
}
