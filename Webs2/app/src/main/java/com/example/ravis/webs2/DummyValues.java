package com.example.ravis.webs2;

import java.util.ArrayList;

/**
 * Created by ravis on 6/7/2017.
 * rules pojo
 */

public class DummyValues {
    ArrayList<DummyData> list=new ArrayList<>();

    DummyValues() {
        list = new ArrayList<>();
        DummyData obj = new DummyData(R.drawable.img1,  "Round up Rule", "Description1", "#C0392B");
        list.add(obj);
        DummyData obj1 = new DummyData(R.drawable.img2, "Spend Less Rule", "Description2", "#A569BD");
        list.add(obj1);
        DummyData obj2 = new DummyData(R.drawable.img3, "Guilty Pleasure Rule", "Description3", "#3498DB");
        list.add(obj2);
        DummyData obj3 = new DummyData(R.drawable.img4, "Set & Forget Rule", "Description4", "#1ABC9C");
        list.add(obj3);
        DummyData obj4 = new DummyData(R.drawable.img5, "52 week Rule", "Description5", "#A6ACAF");
        list.add(obj4);
        DummyData obj5 = new DummyData(R.drawable.img6, "Freelancer Rule", "Description6", "#7D3C98");
        list.add(obj5);
    }
}
