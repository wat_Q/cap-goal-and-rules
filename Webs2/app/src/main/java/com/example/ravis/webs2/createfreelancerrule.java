package com.example.ravis.webs2;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class createfreelancerrule extends AppCompatActivity {
    static Boolean firstclick_morethan = true, firstclick_save = true;
    LinearLayout morethanlayout, savelayout, toyourlayout, towardd;
    Context context;
    Dialog builder;
    TextView dialog_title, dialogmessage, ofmorethan, saveoption;
    EditText dialogedittext;
    Button okbutton, cancelbutton,createfreelancerrulebutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createfreelancerrule);
        context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.createfreelancer_rule_Toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.aleftarrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //initialization of variables
        createfreelancerrulebutton=(Button)findViewById(R.id.createfreelancerrulebutton);
        final helperfunctions helperobject = new helperfunctions();
        morethanlayout = (LinearLayout) findViewById(R.id.morethan);
        ofmorethan = (TextView) findViewById(R.id.ofmorethan);
        savelayout = (LinearLayout) findViewById(R.id.savelayout);
        final LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.customdialog_for_checkbox, null);
        builder = new Dialog(context);
        builder.setContentView(view);
        okbutton = (Button) builder.findViewById(R.id.custom_ok);
        saveoption = (TextView) findViewById(R.id.saveoption);
        cancelbutton = (Button) builder.findViewById(R.id.custom_cancle);
        dialog_title = (TextView) builder.findViewById(R.id.custom_dialog_title);
        dialogmessage = (TextView) builder.findViewById(R.id.custom_dialog_message);
        dialogedittext = (EditText) builder.findViewById(R.id.dialog_edit_text);
        okbutton.setEnabled(false);
        dialogmessage.setTextColor(Color.parseColor("#1FA8CE"));
        dialogedittext.setTextColor(Color.parseColor("#000000"));
        okbutton.setTextColor(Color.parseColor("#b8d7e0"));
        dialogedittext.setHintTextColor(Color.parseColor("#808080"));
        //limits text to only digits
        dialogedittext.setKeyListener(DigitsKeyListener.getInstance(false, false));


        //code for "Of more than custom dialog"
        morethanlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogedittext.setVisibility(View.VISIBLE);
                //component dialog box is always reused so
                // it is checked weather default hint is to be set or custom(recieved from edittext)
                if (firstclick_morethan) {
                    dialogedittext.setHint("$100");
                    firstclick_morethan = false;
                } else
                    dialogedittext.setHint(ofmorethan.getText());

                //user can enter max 4 digits
                helperobject.inputlimiter(dialogedittext, 4);

                dialog_title.setText("Of More Than");
                dialogmessage.setText("Amount(max $5,000)");
                builder.show();

                //validation so that user could enter only <= 5000
                helperobject.limittovalue(5000, 3, dialogedittext, okbutton);

                cancelbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();
                        dialogedittext.setText("");
                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ofmorethan.setText("$" + dialogedittext.getText().toString());
                        dialogedittext.setText("");
                        builder.cancel();
                    }
                });
            }
        });

        savelayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogedittext.setVisibility(View.VISIBLE);
                //component dialog box is always reused so
                // it is checked weather default hint is to be set or custom(recieved from edittext)
                if (firstclick_morethan) {
                    dialogedittext.setHint("30%");
                    firstclick_save = false;
                } else
                    dialogedittext.setHint(saveoption.getText());


                //user can enter max 3 digits
                helperobject.inputlimiter(dialogedittext, 3);
                dialogedittext.setHint(saveoption.getText().toString());

                dialog_title.setText("Save");
                dialogmessage.setText("Percent");
                builder.show();

                //validation so that user could enter only less than or equal to 5000
                helperobject.limittovalue(100, 2, dialogedittext, okbutton);
                cancelbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();
                        dialogedittext.setText("");
                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        saveoption.setText(dialogedittext.getText().toString() + "%");
                        dialogedittext.setText("");
                        builder.cancel();
                    }
                });
            }
        });
        toyourlayout = (LinearLayout) findViewById(R.id.toyour);
        toyourlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*final Dialog dialog = new Dialog(createfreelancerrule.this, R.style.MyDialogTheme);
                dialog.setContentView(R.layout.customdialog_for_checkbox);
                TextView title = (TextView) dialog.findViewById(R.id.custom_dialog_title);
                title.setText("Triggering account");
                TextView message = (TextView) dialog.findViewById(R.id.custom_dialog_message);
                message.setText("This Rule is triggered by deposits of this bank");
                ListView lv = (ListView) dialog.findViewById(R.id.custom_listView);
                EditText customdialogmessage = (EditText) dialog.findViewById(R.id.dialog_edit_text);
                customdialogmessage.setVisibility(View.INVISIBLE);
                Button cancel = (Button) dialog.findViewById(R.id.custom_cancle);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                Button ok = (Button) dialog.findViewById(R.id.custom_ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                dialog.show();
           */

               dialog_title.setText("Triggering account");
                dialogmessage.setText("The Rule will be triggered by deposits to this account");
                //dialogedittext = (EditText) builder.findViewById(R.id.dialog_edit_text);
                dialogedittext.setVisibility(View.INVISIBLE);
                cancelbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();

                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();
                    }
                });
                builder.show(); } });



        toyourlayout = (LinearLayout) findViewById(R.id.towarddd);
        toyourlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* final Dialog dialog = new Dialog(createfreelancerrule.this, R.style.MyDialogTheme);
                dialog.setContentView(R.layout.customdialog_for_checkbox);
                TextView title = (TextView) dialog.findViewById(R.id.custom_dialog_title);
                title.setText("Select Goals");
                TextView message = (TextView) dialog.findViewById(R.id.custom_dialog_message);
                message.setText("Which goal should this rule fund?");
                ListView lv = (ListView) dialog.findViewById(R.id.custom_listView);
                EditText customdialogmessage = (EditText) dialog.findViewById(R.id.dialog_edit_text);
                customdialogmessage.setVisibility(View.INVISIBLE);


                Button cancel = (Button) dialog.findViewById(R.id.custom_cancle);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                Button ok = (Button) dialog.findViewById(R.id.custom_ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                dialog.show();*/
                dialog_title.setText("Select Goals");
                dialogmessage.setText("Which goal should this rule fund?");
                dialogedittext = (EditText) builder.findViewById(R.id.dialog_edit_text);
                dialogedittext.setVisibility(View.INVISIBLE);
                cancelbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();

                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();
                    }
                });
                builder.show();
            }
        });



        createfreelancerrulebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(createfreelancerrule.this,already_created_goals.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.putExtra("position",helperfunctions.helperposition);
                finish();
                startActivity(in);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("pressed","done");
        finish();
    }
}
