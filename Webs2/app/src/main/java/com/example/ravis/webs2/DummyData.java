package com.example.ravis.webs2;

/**
 * Created by ravis on 6/7/2017.
 * Dummy data class for rules
 */

public class DummyData {
    public int img;
    public String title;
    public String desc;
    public String color;

    public int getImg() {
        return img;
    }
    public String getTitle() {
        return title;
    }
    public String getDesc() {
        return desc;
    }
    public String getColor() {
        return color;
    }
    public DummyData(int img, String title, String desc, String color) {
        this.img = img;
        this.title = title;
        this.desc = desc;
        this.color = color;
    }




}


