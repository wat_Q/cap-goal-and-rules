package com.example.ravis.webs2;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by ravis on 6/20/2017.
 */

class helperfunctions {
static int helperposition;
    //user can enter max 4 digits
     void inputlimiter(EditText v, int upto){
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(upto);
        v.setFilters(FilterArray);
    }
//
     void limittovalue(final int limit,final int selection,final EditText dialogedittext,final Button okbutton)
    {
        dialogedittext.addTextChangedListener(new TextWatcher() {

            int savedval, curval;
            String beforestring, afterstring;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                beforestring = dialogedittext.getText().toString();
                if (!((beforestring.equals("")))) {
                    savedval = Integer.parseInt(beforestring);
                }
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                afterstring = dialogedittext.getText().toString();
                if (!afterstring.equals("")) {
                    okbutton.setTextColor(Color.parseColor("#1FA8CE"));
                    curval = Integer.parseInt(afterstring);
                    if (!(curval <= limit)) {
                        dialogedittext.setText(Integer.toString(savedval));
                        dialogedittext.setSelection(selection);
                    }
                    okbutton.setEnabled(true);
                }
                else
                {
                    okbutton.setEnabled(false);
                    okbutton.setTextColor(Color.parseColor("#b8d7e0"));
                }
            }
        });

    }
}

