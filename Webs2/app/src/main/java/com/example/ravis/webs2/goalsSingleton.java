package com.example.ravis.webs2;

import java.util.Objects;

/**
 * Created by ravis on 6/24/2017.
 */

public class goalsSingleton implements Cloneable{
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    private int local_goals_id = 1;
    private String local_goal_name = "";
    private double local_total_amount = 0;
    private double local_current_amount = 0;
    private String local_imagelink = "";

    //object to setData which will be retireved from database(Sqlite)
    private static goalsSingleton obj;

    //object to setValues of goals to send data to Sqlite
    private static goalsSingleton nullGoalsobj;

    //static class that could be accessed from outside to create an instance of this class for "obj"
    public static goalsSingleton createGoalsSingleton(int local_goals_id, String local_goal_name, double local_total_amount, double local_current_amount, String local_imagelink) {
        obj = new goalsSingleton(local_goals_id, local_goal_name, local_total_amount, local_current_amount, local_imagelink);
        return obj;
    }

    //static class that could be accessed from outside to create an instance of this class for "nullGoalsobj"
    public static goalsSingleton nullGoals() {
        nullGoalsobj = new goalsSingleton();
        return nullGoalsobj;
    }
//private constructor that could be used as a raw instance to assign value to its members
    private goalsSingleton() {
    }

    //private parameterized constructor
    private goalsSingleton(int local_goals_id, String local_goal_name, double local_total_amount, double local_current_amount, String local_imagelink) {
        this.local_goals_id = local_goals_id;
        this.local_goal_name = local_goal_name;
        this.local_total_amount = local_total_amount;
        this.local_current_amount = local_current_amount;
        this.local_imagelink = local_imagelink;
    }

    //getters and setters
    public int getLocal_goals_id() {
        return local_goals_id;
    }

    public void setLocal_goals_id(int local_goals_id) {
        this.local_goals_id = local_goals_id;
    }

    public String getLocal_goal_name() {
        return local_goal_name;
    }

    public void setLocal_goal_name(String local_goal_name) {
        this.local_goal_name = local_goal_name;
    }

    public double getLocal_total_amount() {
        return local_total_amount;
    }

    public void setLocal_total_amount(double local_total_amount) {
        this.local_total_amount = local_total_amount;
    }

    public double getLocal_current_amount() {
        return local_current_amount;
    }

    public void setLocal_current_amount(double local_current_amount) {
        this.local_current_amount = local_current_amount;
    }

    public String getLocal_imagelink() {
        return local_imagelink;
    }

    public void setLocal_imagelink(String local_imagelink) {
        this.local_imagelink = local_imagelink;
    }
}
