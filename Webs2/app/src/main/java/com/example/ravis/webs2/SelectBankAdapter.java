package com.example.ravis.webs2;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Kshitij Mittal on 13-06-2017.
 */

public class SelectBankAdapter extends RecyclerView.Adapter<SelectBankAdapter.MyViewHolder>{
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bankname;
        public ImageView icon;
        public LinearLayout mcontainer;


        public MyViewHolder(View view) {
            super(view);
            bankname = (TextView) view.findViewById(R.id.bankname);
            icon = (ImageView) view.findViewById(R.id.bankicon);
            mcontainer=(LinearLayout)view.findViewById(R.id.container);
        }
    }


    public SelectBankAdapter(Context context) {
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.bankcard, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.bankname.setText("Bank "+String.valueOf(position));
        holder.icon.setImageResource(R.drawable.k_account_balance_black);

       holder.mcontainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,FirstPage.class);
                context.startActivity(i);
                ((SelectBank)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }
}
