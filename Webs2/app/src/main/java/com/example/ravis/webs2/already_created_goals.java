package com.example.ravis.webs2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class already_created_goals extends AppCompatActivity {
    ImageButton editgoal;
    ImageView alreadycreatedgoals_imageview;
    TextView newrule_add, alreadycreatedgoals_pageheading, alreadycreatedgoals_maintext, alreadycreatedgoals_subtext;
    ListView ruleschoosen;
    LinearLayout alreadycreatedgoals_newrule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_already_created_goals);
        GoalsPojo goalsPojo = new GoalsPojo();
        alreadycreatedgoals_newrule = (LinearLayout) findViewById(R.id.alreadycreatedgoals_newrule);
        alreadycreatedgoals_pageheading = (TextView) findViewById(R.id.alreadycreatedgoals_pageheading);
        alreadycreatedgoals_imageview = (ImageView) findViewById(R.id.alreadycreatedgoals_imageview);
        alreadycreatedgoals_maintext = (TextView) findViewById(R.id.alreadycreatedgoals_maintext);
        alreadycreatedgoals_subtext = (TextView) findViewById(R.id.alreadycreatedgoals_subtext);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_ofselected_goal);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.aleftarrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

       final int position_in_pojo = getIntent().getIntExtra("position", -1);
        alreadycreatedgoals_pageheading.setText(goalsPojo.arr.get(position_in_pojo).getLocal_goal_name());
        // alreadycreatedgoals_subtext.setText(goalsPojo.arr.get(position_in_pojo).getDetails());
        Picasso.with(already_created_goals.this).load(goalsPojo.arr.get(position_in_pojo).getLocal_imagelink())
                .placeholder(R.drawable.dog)
                .error(android.R.drawable.stat_notify_error)
                .into(alreadycreatedgoals_imageview);

        editgoal = (ImageButton) findViewById(R.id.edit);
        editgoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(already_created_goals.this, Create_edit_goals.class);
                startActivity(i);
            }
        });

       alreadycreatedgoals_newrule.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i = new Intent(already_created_goals.this, createNewRule.class);
               startActivity(i);
           }
       });

        ruleschoosen = (ListView) findViewById(R.id.rulesChoosen);
        alreadycreatedgoals_newrule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helperfunctions.helperposition=position_in_pojo;
                Intent in = new Intent(already_created_goals.this, createNewRule.class);
                startActivity(in);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent in = new Intent(already_created_goals.this, FirstPage.class);
        startActivity(in);

    }
}
