package com.example.ravis.webs2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by aasthu on 13-Jun-17.
 */

public class CustomListViewForChoosenRules extends BaseAdapter {

    private static LayoutInflater inflater=null;
    String rulechoose,ruledescription;
    int Imageofrule;
    Context context;



   public CustomListViewForChoosenRules(String rulechoose,String ruledescription,int Imageofrule)
    {
        this.rulechoose=rulechoose;
        this.ruledescription=ruledescription;
        this.Imageofrule=Imageofrule;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View viewinlist;
        holder holder=new holder();
        viewinlist=inflater.inflate(R.layout.listview_for_alreadycreatedgoal,null);
        holder.nameofrule=(TextView)viewinlist.findViewById(R.id.rulename);
        holder.descriptionofrule=(TextView)viewinlist.findViewById(R.id.descriptionofrule);
        holder.ruleimage=(ImageView)viewinlist.findViewById(R.id.ruleimage);
        holder.nameofrule.setText("");
        holder.descriptionofrule.setText("");
        holder.ruleimage.setImageResource(Integer.parseInt(null));
        return null;
    }

    public  class holder{
        TextView nameofrule,descriptionofrule;
        ImageView ruleimage;

    }


}
