package com.example.ravis.webs2;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GuiltyPleasureRule extends AppCompatActivity {
    LinearLayout selectMerchent,linkedbank,saveamount,selectGoal,fundedaccount;
    ImageView backbutton;
    final String[] bankvalue={"Bank 1","Bank 2","Bank 3","Bank 4","Bank 5"};
    final String[] goalvalue={"Goal 1","Goal 2","Goal 3","Goal 4","Goal 5"};
    boolean[] selected={false,true,false,false,true};
    int[] image={R.drawable.img1,R.drawable.img2,R.drawable.img3,R.drawable.img4,R.drawable.img5};
    int set=2,limit=500,preset=30;
    AlertDialogs alert;
    Button createguiltyrulebutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guilty_pleasure_rule);

        alert=new AlertDialogs(GuiltyPleasureRule.this);

        backbutton=(ImageView)findViewById(R.id.back);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("response"));
createguiltyrulebutton=(Button)findViewById(R.id.gpcreaterule);
createguiltyrulebutton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent in=new Intent(GuiltyPleasureRule.this,already_created_goals.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in.putExtra("position",helperfunctions.helperposition);
        finish();
        startActivity(in);
    }
});
        init();
    }

    void init(){

        selectMerchent=(LinearLayout)findViewById(R.id.using_atmerchant);
        selectMerchent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(GuiltyPleasureRule.this,SelectGuiltyMerchant.class);
                startActivity(i);
            }
        });


        linkedbank=(LinearLayout)findViewById(R.id.using_linkedbank);
        final TextView gplinkedbank = (TextView) findViewById(R.id.gplinkedbank);
        linkedbank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.SingleSelectWithImage("Select Funded Bank",bankvalue,image,new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String x=bankvalue[which];
                        gplinkedbank.setText(x);
                    }
                });
            }
        });
        saveamount=(LinearLayout)findViewById(R.id.using_saveamount);
        final TextView gpcurrentsave=(TextView)findViewById(R.id.gpcurrentsave);
        gpcurrentsave.setText("₹"+String.valueOf(preset));
        saveamount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                alert.editTextField("SAVE", "Max limit = ₹"+String.valueOf(limit), preset, limit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int x = alert.getVal();
                        gpcurrentsave.setText("₹"+String.valueOf(x));
                    }
                });
            }
        });
        selectGoal=(LinearLayout)findViewById(R.id.using_towardsgoal);
        selectGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  alert.MultiSelectDialogCheck("goal",goalvalue,selected,image);
            }
        });


        fundedaccount=(LinearLayout)findViewById(R.id.using_fundedaccount);
        final TextView gpfundedbank=(TextView)findViewById(R.id.gpfundedbank);
        fundedaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.SingleSelectWithImage("Select Funded Bank",bankvalue,image,new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String x=bankvalue[which];
                        gpfundedbank.setText(x);
                    }
                });
            }
        });
    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            selected = intent.getBooleanArrayExtra("isselected");
        }
    };
}
