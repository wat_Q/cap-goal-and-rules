package com.example.ravis.webs2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

/**
 * Created by ravis on 6/13/2017.
 */

public class firstpageRviewAdapter extends RecyclerView.Adapter<firstpageRviewAdapter.ViewHolder> {
    Context context;
    GoalsPojo pojo;

    public firstpageRviewAdapter(Context context, GoalsPojo pojo) {
        this.context = context;
        this.pojo = pojo;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView cardViewImage;
        public TextView cardheading;
        public TextView card_details;
        public CardView cardView;
        public ViewHolder(View itemView) {
            super(itemView);
            cardViewImage=(ImageView) itemView.findViewById(R.id.firstpagecardviewimage);
            cardheading=(TextView)itemView.findViewById(R.id.firstpagecardheading);
            card_details=(TextView)itemView.findViewById(R.id.firstpagecardtext);
            cardView=(CardView)itemView.findViewById(R.id.firstpageCardVIew);

        }
    }

    @Override
    public firstpageRviewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view =inflater.from(parent.getContext()).inflate(R.layout.firstpagecardview,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.cardheading.setText(pojo.arr.get(position).getLocal_goal_name());
        holder.card_details.setText(pojo.arr.get(position).getLocal_current_amount()+"");

        Picasso.with(context).load(pojo.arr.get(position).getLocal_imagelink())
                .placeholder(R.drawable.dog)
                .error(android.R.drawable.stat_notify_error)
                .into(holder.cardViewImage);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(context,already_created_goals.class);
                in.putExtra("position",position);
                Log.d("msg",position+"");
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(in);
            }
        });
    }



    @Override
    public int getItemCount() {
        return pojo.arr.size();
    }
}
