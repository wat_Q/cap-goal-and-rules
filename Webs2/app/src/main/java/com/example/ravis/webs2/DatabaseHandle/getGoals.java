package com.example.ravis.webs2.DatabaseHandle;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.ravis.webs2.goalsSingleton;
import com.example.ravis.webs2.pojo.Goals;
import com.example.ravis.webs2.selectGoals_fields;

import java.util.ArrayList;

/**
 * Created by ravis on 6/28/2017.
 */

public class getGoals extends SQLiteOpenHelper implements Cloneable {
    ArrayList<Goals> user_goals_Array = new ArrayList<>();
    public static final String DATABASE_NAME = "capgoalrules.db";

    //table name
    public static final String TABLE_GOALS = "goals";
    public static final String TABLE_RULES = "rules";
    public static final String TABLE_MAPPING = "mapping";

    //Goals column names
    public static final String GOAL_ID = "goalid"; //mapping column name

    public static final String GOAL_NAME = "goalname";
    public static final String GOAL_COLLECTED_AMOUNT = "collectedamount";
    public static final String GOAL_TOTAL_AMOUNT = "totalamount";
    public static final String GOAL_IMAGE = "images";
    public static final String CREATE_GOAL_TABLE =
            "CREATE TABLE " + TABLE_GOALS + "( "
                    + GOAL_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                    + GOAL_NAME + " TEXT, "
                    + GOAL_COLLECTED_AMOUNT + " REAL, "
                    + GOAL_TOTAL_AMOUNT + " REAL, "
                    + GOAL_IMAGE + " TEXT ); ";


    public getGoals(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_GOAL_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GOALS);
        onCreate(db);
    }

    public Cursor getGoalsFromDatabase() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_GOALS, null);
        return cursor;
    }

    public ArrayList<Goals> getDatafromCursor() {

        SQLiteDatabase mydb = this.getReadableDatabase();
        Cursor res = getGoalsFromDatabase();
        if (res.getCount() == 0) {
            return null;
        }

        while (res.moveToNext()) {
            Goals copy = new Goals();
            copy.setLocal_goals_id(Integer.parseInt(res.getString(0)));
            copy.setLocal_goal_name(res.getString(1));
            copy.setLocal_current_amount(Double.parseDouble(res.getString(2)));
            copy.setLocal_total_amount(Double.parseDouble(res.getString(3)));
            copy.setLocal_imagelink(res.getString(4));

          user_goals_Array.add(copy);
        }
        return user_goals_Array;
    }
}
