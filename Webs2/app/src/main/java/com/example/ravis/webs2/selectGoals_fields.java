package com.example.ravis.webs2;

/**
 * Created by ravis on 6/13/2017.
 */

public class selectGoals_fields {
    public int img;
    public String text;
    public String details;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }



    public selectGoals_fields(int img, String text,String details) {
        this.img = img;
        this.text = text;
        this.details=details;

    }

    public int getImg() {
        return img;
    }

    public String getText() {
        return text;
    }

    public void setImg(int img) {
        this.img = img;
     }

    public void setText(String text) {
        this.text = text;
    }


}
