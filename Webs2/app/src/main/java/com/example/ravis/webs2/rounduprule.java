package com.example.ravis.webs2;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class rounduprule extends AppCompatActivity {
    LinearLayout using_linkedbanks,roundupamount,goalselection,yourLinkedBanks;
    LayoutInflater layoutInflater;
    Context context;
    Button createrounduprulebutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.round_uprule);
       context=rounduprule.this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_ofroundup_rule);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.aleftarrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
createrounduprulebutton=(Button)findViewById(R.id.createrounduprulebutton);
        createrounduprulebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(rounduprule.this,already_created_goals.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.putExtra("position",helperfunctions.helperposition);
                finish();
                startActivity(in);
            }
        });
          layoutInflater= (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        using_linkedbanks=(LinearLayout)findViewById(R.id.using_linkedbank);
        using_linkedbanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(rounduprule.this,R.style.MyDialogTheme);
                alertDialogBuilder.setMessage("This rule is triggered by activity from these accounts:")
                .setTitle("Triggering account")
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        Toast.makeText(rounduprule.this,"You clicked yes button",Toast.LENGTH_LONG).show();
                                    }
                                }).create();
                   alertDialogBuilder.show();
            }
        });
        final String[] values={"1.00","2.00","3.00","4.00","5.00"};
final TextView roundupvalue=(TextView)findViewById(R.id.roundupvalue);
        roundupamount=(LinearLayout)findViewById(R.id.roundupamount);
        roundupamount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*View dialogView = layoutInflater.inflate(R.layout.customdialog_for_checkbox,null);
                final Dialog dialog=new Dialog(rounduprule.this,R.style.MyDialogTheme);
                dialog.setContentView(dialogView);
                TextView title=(TextView)dialog.findViewById(R.id.custom_dialog_title);
                title.setText("Round up to");
                EditText editTexttobehidden=(EditText)dialog.findViewById(R.id.dialog_edit_text);
                editTexttobehidden.setVisibility(View.INVISIBLE);
                TextView message=(TextView)dialog.findViewById(R.id.custom_dialog_message);
                message.setVisibility(View.INVISIBLE);
                ListView lv=(ListView)dialog.findViewById(R.id.custom_listView);
                final CustomListViewforrounduprule cc=new CustomListViewforrounduprule(getApplicationContext(), values);
                lv.setAdapter(cc);
                Button cancel=(Button)dialog.findViewById(R.id.custom_cancle);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                Button ok=(Button)dialog.findViewById(R.id.custom_ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       String a=  cc.getSelectedItem();
                        roundupvalue.setText(a);
                        dialog.cancel();
                    }
                });
                dialog.show();*/
                classforsingleandmultiselect d=new classforsingleandmultiselect(rounduprule.this,roundupvalue,values,"Round up to");
                d.singlechoiceselection();
            }

            });

        goalselection=(LinearLayout)findViewById(R.id.gaolselection);
        goalselection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog=new Dialog(rounduprule.this,R.style.MyDialogTheme);
                dialog.setContentView(R.layout.customdialog_for_checkbox);
                TextView title=(TextView)dialog.findViewById(R.id.custom_dialog_title);
                title.setText("Select Goals");
                TextView message=(TextView)dialog.findViewById(R.id.custom_dialog_message);
                message.setText("Which goal should this rule fund?");
                ListView lv=(ListView)dialog.findViewById(R.id.custom_listView);
                EditText customdialogmessage=(EditText)dialog.findViewById(R.id.dialog_edit_text);
                customdialogmessage.setVisibility(View.INVISIBLE);


                Button cancel=(Button)dialog.findViewById(R.id.custom_cancle);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                Button ok=(Button)dialog.findViewById(R.id.custom_ok);
               ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                dialog.show();
            }
        });
        yourLinkedBanks=(LinearLayout)findViewById(R.id.your_linkedbank);
        yourLinkedBanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog=new Dialog(rounduprule.this,R.style.MyDialogTheme);
                dialog.setContentView(R.layout.customdialog_for_checkbox);
                TextView title=(TextView)dialog.findViewById(R.id.custom_dialog_title);
                title.setText("Funding Banks");
                TextView message=(TextView)dialog.findViewById(R.id.custom_dialog_message);
                message.setText("This Rule is funded from this bank");
                ListView lv=(ListView)dialog.findViewById(R.id.custom_listView);
                EditText customdialogmessage=(EditText)dialog.findViewById(R.id.dialog_edit_text);
                customdialogmessage.setVisibility(View.INVISIBLE);
                Button cancel=(Button)dialog.findViewById(R.id.custom_cancle);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                Button ok=(Button)dialog.findViewById(R.id.custom_ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                dialog.show();}
            });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
