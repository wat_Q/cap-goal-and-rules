package com.example.ravis.webs2;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.FUNDED_BANK;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_DURATION;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_SAVE_AMOUNT;

public class CreateForgetRule extends AppCompatActivity {
    LinearLayout saveOption, duration, selectGoal, fundedaccount;
    ImageView backbutton;
    ContentValues dbvalues;
    final String[] values = {"week", "month", "year"};
    final String[] bankvalue = {"Bank 1", "Bank 2", "Bank 3", "Bank 4", "Bank 5"};
    final String[] goalvalue = {"Goal 1", "Goal 2", "Goal 3", "Goal 4", "Goal 5"};
    boolean[] selected = {false, true, false, false, true};
    int[] image = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5};
    int set = 0, limit = 500, preset = 30;
    AlertDialogs alert;
    Button createforgetrulebutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_forget_rule);

        alert = new AlertDialogs(CreateForgetRule.this);
        dbvalues=new ContentValues();
        dbvalues.put(RULE_NAME,"Create & Forget Rule");
        dbvalues.put(RULE_IMAGE,R.drawable.img2);
        backbutton = (ImageView) findViewById(R.id.back);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("response"));

        init();
        createforgetrulebutton = (Button) findViewById(R.id.createrule);
        createforgetrulebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(CreateForgetRule.this, already_created_goals.class);
               // db.insertRule(dbvalues);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.putExtra("position", helperfunctions.helperposition);
                finish();
                startActivity(in);
            }
        });
    }

    public void init() {
        saveOption = (LinearLayout) findViewById(R.id.using_saveamount);
        final TextView sfsaveamount = (TextView) findViewById(R.id.sfsaveamount);
        saveOption.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                alert.editTextField("SAVE", "Max limit = ₹" + String.valueOf(limit), preset, limit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sfsaveamount.setText("₹" + String.valueOf(alert.getVal()));
                        dbvalues.put(RULE_SAVE_AMOUNT,alert.getVal());
                    }
                });
            }
        });


        duration = (LinearLayout) findViewById(R.id.set_interval);
        final TextView sfper = (TextView) findViewById(R.id.sfper);
        duration.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                alert.SingleSelectDialog("Duration", set, values, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        set = alert.getAmount() - 1;
                        dbvalues.put(RULE_DURATION,set);
                        sfper.setText(values[set]);
                    }
                });
            }
        });


        selectGoal = (LinearLayout) findViewById(R.id.using_towardsgoal);
        selectGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  alert.MultiSelectDialogCheck("Select Goals", goalvalue, selected, image);
            }
        });


        fundedaccount = (LinearLayout) findViewById(R.id.funded_account);
        final TextView sffundbank = (TextView) findViewById(R.id.sffundbank);
        fundedaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.SingleSelectWithImage("Select Funded Bank", bankvalue, image, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                         dbvalues.put(FUNDED_BANK, bankvalue[which]);
                        sffundbank.setText(bankvalue[which]);
                    }
                });
            }
        });

    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            selected = intent.getBooleanArrayExtra("isselected");
        }
    };


}
