package com.example.ravis.webs2.pojo;

import java.util.List;

public class rules {

    private int ruleid = 0, saveamount = 0, savedamount = 0, duration = 0, savepercentage = 0, ruleimage=0;
    private String rulename = null, merchant = null, fundedbank = null, tiggeringbank = null;
    private List<Integer> towardsgoalids = null;
    private boolean weekorder = false, ispaused = false;

    private static rules roundup;
    private static rules spendless;
    private static rules guiltypleasure;
    private static rules setforgetrule;
    private static rules rule_52week;
    private static rules freelancerRule;
    private static rules nullruleobject;


    //ensures creation of one object only
    public static       rules RoundupRule(int ruleid, String rulename, String tiggeringbank, int saveamount, List<Integer> towardsgoalids, String fundedbank) {
        roundup = new   rules(ruleid, rulename, tiggeringbank, saveamount, towardsgoalids, fundedbank);
        return roundup;
    }

    //ensures creation of one object only
    public static rules spendless(int ruleid, String rulename, int saveamount, int duration, String merchant, String tiggeringbank, List<Integer> towardsgoalids, String fundedbank) {
        spendless = new rules(ruleid, rulename, saveamount, duration, merchant, tiggeringbank, towardsgoalids, fundedbank);
        return spendless;
    }

    //ensures creation of one object only
    public static rules guiltypleasure(int ruleid, String rulename, String merchant, String tiggeringbank, int saveamount, List<Integer> towardsgoalids, String fundedbank) {
        guiltypleasure = new rules(ruleid, rulename, merchant, tiggeringbank, saveamount, towardsgoalids, fundedbank);
        return guiltypleasure;
    }

    //ensures creation of one object only
    public static rules setforget(int ruleid, String rulename, int saveamount, int duration, List<Integer> towardsgoalids, String fundedbank) {
        setforgetrule = new rules(ruleid, rulename, saveamount, duration, towardsgoalids, fundedbank);
        return setforgetrule;
    }

    //ensures creation of one object only
    public static rules fiftytwoweek(int ruleid, String rulename, int duration, boolean weekorder, List<Integer> towardsgoalids, String fundedbank) {
        rule_52week = new rules(ruleid, rulename, duration, weekorder, towardsgoalids, fundedbank);
        return rule_52week;
    }

    //ensures creation of one object only
    public static rules freelancer(int ruleid, String rulename, String tiggeringbank, int saveamount, int savepercentage, List<Integer> towardsgoalids) {
        freelancerRule = new rules(ruleid, rulename, tiggeringbank, saveamount, savepercentage, towardsgoalids);
        return freelancerRule;
    }

    //null object
    public static rules nullrules() {
        nullruleobject = new rules();
        return nullruleobject;
    }

    //round up rule
    private rules(int ruleid, String rulename, String tiggeringbank, int saveamount, List<Integer> towardsgoalids, String fundedbank) {
        this.ruleid = ruleid;
        this.saveamount = saveamount;
        this.rulename = rulename;
        this.fundedbank = fundedbank;
        this.tiggeringbank = tiggeringbank;
        this.towardsgoalids = towardsgoalids;
    }

    //null reference
    private rules() {

    }


    //for spend less rule
    private rules(int ruleid, String rulename, int saveamount, int duration, String merchant, String tiggeringbank, List<Integer> towardsgoalids, String fundedbank) {
        this.ruleid = ruleid;
        this.saveamount = saveamount;
        this.duration = duration;
        this.rulename = rulename;
        this.merchant = merchant;
        this.fundedbank = fundedbank;
        this.tiggeringbank = tiggeringbank;
        this.towardsgoalids = towardsgoalids;
    }

    //for guilty pleasure rule
    private rules(int ruleid, String rulename, String merchant, String tiggeringbank, int saveamount, List<Integer> towardsgoalids, String fundedbank) {
        this.ruleid = ruleid;
        this.saveamount = saveamount;
        this.rulename = rulename;
        this.merchant = merchant;
        this.fundedbank = fundedbank;
        this.tiggeringbank = tiggeringbank;
        this.towardsgoalids = towardsgoalids;
    }

    //for set & forget rule
    private rules(int ruleid, String rulename, int saveamount, int duration, List<Integer> towardsgoalids, String fundedbank) {
        this.ruleid = ruleid;
        this.saveamount = saveamount;
        this.duration = duration;
        this.rulename = rulename;
        this.fundedbank = fundedbank;
        this.towardsgoalids = towardsgoalids;
    }

    //for 52 week rule
    private rules(int ruleid, String rulename, int duration, boolean weekorder, List<Integer> towardsgoalids, String fundedbank) {
        this.ruleid = ruleid;
        this.duration = duration;
        this.rulename = rulename;
        this.fundedbank = fundedbank;
        this.towardsgoalids = towardsgoalids;
        this.weekorder = weekorder;
    }

    //for freelancer rule
    private rules(int ruleid, String rulename, String tiggeringbank, int saveamount, int savepercentage, List<Integer> towardsgoalids) {
        this.ruleid = ruleid;
        this.saveamount = saveamount;
        this.savedamount = 0;
        this.duration = 0;
        this.savepercentage = savepercentage;
        this.rulename = rulename;
        this.tiggeringbank = tiggeringbank;
        this.towardsgoalids = towardsgoalids;
    }


    public int getId() {
        return ruleid;
    }
    public void setId(int ruleid) {
        this.ruleid = ruleid;
    }

    public int getAmount() {
        return saveamount;
    }
    public void setAmount(int saveamount) {
        this.saveamount = saveamount;
    }

    public int getSavedamount() {
        return savedamount;
    }
    public void setSavedamount(int savedamount) {
        this.savedamount = savedamount;
    }

    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getSavepercent() {
        return savepercentage;
    }
    public void setSavepercent(int savepercentage) {
        this.savepercentage = savepercentage;
    }

    public String getrulename() {
        return rulename;
    }
    public void setrulename(String rulename) {
        this.rulename = rulename;
    }

    public String getMerchant() {return merchant;}
    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getFundingbank() {
        return fundedbank;
    }
    public void setFundingbank(String fundedbank) {
        this.fundedbank = fundedbank;
    }

    public String getTiggeringbank() {
        return tiggeringbank;
    }
    public void setTiggeringbank(String tiggeringbank) {
        this.tiggeringbank = tiggeringbank;
    }

    public List<Integer> gettowardsgoalids() {
        return towardsgoalids;
    }
    public void settowardsgoalids(List<Integer> towardsgoalids) {this.towardsgoalids = towardsgoalids;}

    public boolean isOrder() {
        return weekorder;
    }
    public void setOrder(boolean weekorder) {
        this.weekorder = weekorder;
    }

    public boolean ispaused() {return ispaused;}
    public void setIspaused(boolean ispaused) {this.ispaused = ispaused;}

    public int getRuleimage() {return ruleimage;}
    public void setRuleimage(int ruleimage) {this.ruleimage = ruleimage;}
}
