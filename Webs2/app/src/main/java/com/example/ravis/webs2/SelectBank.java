package com.example.ravis.webs2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


public class SelectBank extends AppCompatActivity {
    private RecyclerView recyclerView;
    private SelectBankAdapter mAdapter;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_bank);
context=this;
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mAdapter = new SelectBankAdapter(context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(SelectBank.this,Setup.class);
        finish();
        startActivity(i);
    }
}
