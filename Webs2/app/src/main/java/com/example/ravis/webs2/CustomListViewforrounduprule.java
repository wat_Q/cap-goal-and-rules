package com.example.ravis.webs2;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by aasthu on 15-Jun-17.
 */

class CustomListViewforrounduprule extends BaseAdapter {
    String [] result;
    int selectedPosition=0;
    Context context;
    private static LayoutInflater inflater=null;
    public CustomListViewforrounduprule(Context context, String[] prgmNameList) {
        result=prgmNameList;
        this.context=context;

        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView tv;
        ImageView img;
        CheckBox checkBoxtobehidden;
        RadioButton radioButton;
        RadioGroup radioGroup;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.customlistview_for_customdialog_box, null);
        holder.tv=(TextView) rowView.findViewById(R.id.textaccordingtorule);
        holder.tv.setVisibility(View.INVISIBLE);
        holder.radioGroup=(RadioGroup) rowView.findViewById(R.id.forsingleslection) ;
        holder.radioButton=(RadioButton)rowView.findViewById(R.id.radioButtontobediaplayed) ;

        holder.radioButton.setTag(position);

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( holder.radioButton.isChecked())
                {
                    itemCheckChanged(v);
                }

            }
        });



       holder.radioButton.setText(result[position]+" ");
        holder.radioButton.setTextColor(Color.BLACK);
        holder.radioButton.setTextSize(15);
        holder.radioButton.setChecked(position == selectedPosition);


        holder.img=(ImageView) rowView.findViewById(R.id.tobehiddenimage);
        holder.img.setVisibility(View.INVISIBLE);

        holder.checkBoxtobehidden=(CheckBox)rowView.findViewById(R.id.checkBoxtobe);
        holder.checkBoxtobehidden.setVisibility(View.INVISIBLE);
        return rowView;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    public String getSelectedItem() {
        if (selectedPosition != -1) {
            return result[selectedPosition];

        }
        return  "";
    }

}

