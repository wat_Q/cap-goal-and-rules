package com.example.ravis.webs2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Setup extends AppCompatActivity {
Button continuebutton;
    Context firstpagecontext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        continuebutton=(Button)findViewById(R.id.button);
        Intent in=this.getIntent();
        //firstpagecontext=(Context)in.getStringExtra("firstpagecontext");
        continuebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onContinue(v);
            }
        });

    }

    public void onContinue(View v){
        Intent i=new Intent(Setup.this,SelectBank.class);
        finish();
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(Setup.this,FirstPage.class);
        finish();
        startActivity(i);
    }

}
