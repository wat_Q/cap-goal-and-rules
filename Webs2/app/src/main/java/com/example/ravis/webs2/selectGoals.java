package com.example.ravis.webs2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

/**
 * Created by ravis on 6/13/2017.
 */

public class selectGoals extends AppCompatActivity {
RecyclerView goalsRecyclerView;
    RecyclerView.Adapter selectGoalsRviewAdapter;
    Toolbar t;
CardView goalsCardView;
    RecyclerView.LayoutManager manager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectgoals);
        GoalsPojo pojo=new GoalsPojo();
        t=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(t);
        if(getSupportActionBar()!=null)
        {
            //uncomment below to enable back button on toolbar
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        goalsRecyclerView=(RecyclerView)findViewById(R.id.selectGoalsRecyclerView);
        goalsCardView=(CardView)findViewById(R.id.selectgoalsCardVIew);
        manager=new LinearLayoutManager(this);
        goalsRecyclerView.setNestedScrollingEnabled(false);
        selectGoalsRviewAdapter=new goalsAdapter(this,pojo);
        goalsRecyclerView.setAdapter(selectGoalsRviewAdapter);
        goalsRecyclerView.setLayoutManager(manager);


    }
}
