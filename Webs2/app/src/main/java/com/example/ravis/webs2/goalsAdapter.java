package com.example.ravis.webs2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by ravis on 6/13/2017.
 */

public class goalsAdapter extends RecyclerView.Adapter<goalsAdapter.ViewHolder> {
    Context context;
    GoalsPojo pojo;

    public goalsAdapter(Context context, GoalsPojo pojo) {
        this.context = context;
        this.pojo = pojo;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView cardViewImage;
        public TextView textView;
        public CardView selectgoals_cardview;
        public ViewHolder(View itemView) {
            super(itemView);
            cardViewImage=(ImageView) itemView.findViewById(R.id.cardviewimage);
            textView=(TextView)itemView.findViewById(R.id.textongoals);
selectgoals_cardview=(CardView)itemView.findViewById(R.id.selectgoalsCardVIew);
        }
    }

    @Override
    public goalsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View view =inflater.from(parent.getContext()).inflate(R.layout.select_goals_card_view,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textView.setText(pojo.arr.get(position).getLocal_goal_name());

        Picasso.with(context).load(pojo.arr.get(position).getLocal_imagelink())
                .placeholder(R.drawable.dog)
                .error(android.R.drawable.stat_notify_error)
                .into(holder.cardViewImage);
        holder.selectgoals_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
    }



    @Override
    public int getItemCount() {
        return pojo.arr.size();
    }
}
