package com.example.ravis.webs2.pojo;

/**
 * Created by ravis on 6/24/2017.
 */

public class Goals {
    public int local_goals_id = 0;
    public String local_goal_name = "";
    public double local_total_amount = 0;
    public double local_current_amount = 0;
    public String local_imagelink = "";

    public int getLocal_goals_id() {
        return local_goals_id;
    }

    public void setLocal_goals_id(int local_goals_id) {
        this.local_goals_id = local_goals_id;
    }

    public String getLocal_goal_name() {
        return local_goal_name;
    }

    public void setLocal_goal_name(String local_goal_name) {
        this.local_goal_name = local_goal_name;
    }

    public double getLocal_total_amount() {
        return local_total_amount;
    }

    public void setLocal_total_amount(double local_total_amount) {
        this.local_total_amount = local_total_amount;
    }

    public double getLocal_current_amount() {
        return local_current_amount;
    }

    public void setLocal_current_amount(double local_current_amount) {
        this.local_current_amount = local_current_amount;
    }

    public String getLocal_imagelink() {
        return local_imagelink;
    }

    public void setLocal_imagelink(String local_imagelink) {
        this.local_imagelink = local_imagelink;
    }
}
