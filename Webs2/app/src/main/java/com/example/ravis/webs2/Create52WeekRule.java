package com.example.ravis.webs2;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Create52WeekRule extends AppCompatActivity {
    LinearLayout towardss,your,starton,orderthestart;
    LayoutInflater layoutInflater;
    Button create52weekrulebutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create52_week_rule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.create52weekToolbar);
        create52weekrulebutton=(Button)findViewById(R.id.create52weekrulebutton);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.aleftarrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        towardss=(LinearLayout)findViewById(R.id.towardss);
        layoutInflater= (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        towardss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog=new Dialog(Create52WeekRule.this,R.style.MyDialogTheme);
                dialog.setContentView(R.layout.customdialog_for_checkbox);
                TextView title=(TextView)dialog.findViewById(R.id.custom_dialog_title);
                title.setText("Select Goals");
                TextView message=(TextView)dialog.findViewById(R.id.custom_dialog_message);
                message.setText("Which goal should this rule fund?");
                ListView lv=(ListView)dialog.findViewById(R.id.custom_listView);
                EditText customdialogmessage=(EditText)dialog.findViewById(R.id.dialog_edit_text);
                customdialogmessage.setVisibility(View.INVISIBLE);


                Button cancel=(Button)dialog.findViewById(R.id.custom_cancle);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
dialog.cancel();
                    }
                });

                Button ok=(Button)dialog.findViewById(R.id.custom_ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
dialog.cancel();
                    }
                });
                dialog.show();
            }
        });
        your=(LinearLayout)findViewById(R.id.yourbank);
        your.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog=new Dialog(Create52WeekRule.this,R.style.MyDialogTheme);
                dialog.setContentView(R.layout.customdialog_for_checkbox);
                TextView title=(TextView)dialog.findViewById(R.id.custom_dialog_title);
                title.setText("Funding Banks");
                TextView message=(TextView)dialog.findViewById(R.id.custom_dialog_message);
                message.setText("This Rule is funded from this bank");
                ListView lv=(ListView)dialog.findViewById(R.id.custom_listView);
                EditText customdialogmessage=(EditText)dialog.findViewById(R.id.dialog_edit_text);
                customdialogmessage.setVisibility(View.INVISIBLE);
                Button cancel=(Button)dialog.findViewById(R.id.custom_cancle);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                Button ok=(Button)dialog.findViewById(R.id.custom_ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();}
        });

       starton=(LinearLayout)findViewById(R.id.starton);
        final String[] values=new String[52];
        for(int i=0;i<52;i++)
        {
            int a=i+1;
            values[i]="week"+a+"- $"+a;
        }
        final TextView weekvalue=(TextView)findViewById(R.id.weekvalue);

        starton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Toast.makeText(Create52WeekRule.this,"hello",Toast.LENGTH_LONG).show();
                View dialogView = layoutInflater.inflate(R.layout.customdialog_for_checkbox,null);
                final Dialog dialog=new Dialog(Create52WeekRule.this,R.style.MyDialogTheme);
                dialog.setContentView(dialogView);
                TextView title=(TextView)dialog.findViewById(R.id.custom_dialog_title);
                title.setText("Round up to");
                EditText editTexttobehidden=(EditText)dialog.findViewById(R.id.dialog_edit_text);
                editTexttobehidden.setVisibility(View.INVISIBLE);
                TextView message=(TextView)dialog.findViewById(R.id.custom_dialog_message);
                message.setVisibility(View.INVISIBLE);
                ListView lv=(ListView)dialog.findViewById(R.id.custom_listView);
                final CustomListViewforrounduprule cc=new CustomListViewforrounduprule(getApplicationContext(), values);
                lv.setAdapter(cc);
                Button cancel=(Button)dialog.findViewById(R.id.custom_cancle);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                Button ok=(Button)dialog.findViewById(R.id.custom_ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String a=  cc.getSelectedItem();
                        weekvalue.setText(a);
                        dialog.cancel();
                    }
                });
                dialog.show();*/
                classforsingleandmultiselect d=new classforsingleandmultiselect(Create52WeekRule.this,weekvalue,values,"Select order");
                d.singlechoiceselection();
            }

        });



       orderthestart=(LinearLayout)findViewById(R.id.orderthestart);
        final TextView ordervalue=(TextView)findViewById(R.id.ordervalue);
final String[] val={"regular(Start with $1)","reversed (start with $52)"};
        orderthestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Toast.makeText(Create52WeekRule.this,"hello",Toast.LENGTH_LONG).show();
                View dialogView = layoutInflater.inflate(R.layout.customdialog_for_checkbox,null);
                final Dialog dialog=new Dialog(Create52WeekRule.this,R.style.MyDialogTheme);
                dialog.setContentView(dialogView);
                TextView title=(TextView)dialog.findViewById(R.id.custom_dialog_title);
                title.setText("Select Order");
                EditText editTexttobehidden=(EditText)dialog.findViewById(R.id.dialog_edit_text);
                editTexttobehidden.setVisibility(View.INVISIBLE);
                TextView message=(TextView)dialog.findViewById(R.id.custom_dialog_message);
                message.setVisibility(View.INVISIBLE);
                ListView lv=(ListView)dialog.findViewById(R.id.custom_listView);
                final CustomListViewforrounduprule cc=new CustomListViewforrounduprule(getApplicationContext(), val);
                lv.setAdapter(cc);
                Button cancel=(Button)dialog.findViewById(R.id.custom_cancle);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

                Button ok=(Button)dialog.findViewById(R.id.custom_ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String a=  cc.getSelectedItem();
                        ordervalue.setText(a);
                        dialog.cancel();
                    }
                });
                dialog.show();*/
                classforsingleandmultiselect d=new classforsingleandmultiselect(Create52WeekRule.this,ordervalue,val,"Select order");
                d.singlechoiceselection();
            }

        });
create52weekrulebutton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent in=new Intent(Create52WeekRule.this,already_created_goals.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        in.putExtra("position",helperfunctions.helperposition);
        finish();
        startActivity(in);
    }
});

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
