package com.example.ravis.webs2;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;


public class AlertDialogs {

    private Context context;
    private int val;
    private int amount;

    public AlertDialogs(Context context) {
        this.context = context;
    }


    //FOR EDITEXT
    public void editTextField(String head, String subhead, final int preset, final int limit, DialogInterface.OnClickListener pressok) {
        val = preset;


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialogBuilder.setTitle(head);
        dialogBuilder.setMessage(subhead);
        LinearLayout temp = new LinearLayout(context);
        temp.setOrientation(LinearLayout.HORIZONTAL);
        temp.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        final EditText response = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(60,0,60,0);
        response.setLayoutParams(lp);
        response.setInputType(InputType.TYPE_CLASS_NUMBER);
        response.setHint("₹"+String.valueOf(val));
        temp.addView(response);

        dialogBuilder.setView(temp);


        response.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                String x=response.getText().toString();
                if(!x.equals("")){
                    val=Integer.valueOf(x);
                }else{val=0;}
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int z;
                if(!s.toString().equals("")) {
                    z = Integer.valueOf(s.toString());
                }else{z=preset;}
                if(z<(limit+1)){
                    val=z;
                }else{
                    response.setText(String.valueOf(val));
                    response.setSelection(start);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        dialogBuilder.setPositiveButton("ok",pressok);
        dialogBuilder.setNegativeButton("Cancel",null);
        dialogBuilder.show();
    }


    //FOR SINGLE SELECT
    public void SingleSelectDialog(String head, int preset, final String[] values, DialogInterface.OnClickListener pressok) {
        amount=preset+1;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialogBuilder.setTitle(head);
        dialogBuilder.setSingleChoiceItems(values, preset, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                amount = arg1+1;

            }
        });
        dialogBuilder.setPositiveButton("ok", pressok);
        dialogBuilder.setNegativeButton("Cancel", null);
        dialogBuilder.show();

    }

    //for Single Select With Image
    public void SingleSelectWithImage(String head, final String[] values, int[] image, DialogInterface.OnClickListener pressok) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialogBuilder.setTitle(head);
        final MultiSelectAdapter adapter = new MultiSelectAdapter(context, values, image);
        dialogBuilder.setAdapter(adapter, pressok);
        dialogBuilder.setNegativeButton("Cancel", null);
        dialogBuilder.show();

    }


    //for Multi Select with checkbox
    public void MultiSelectDialogCheck(String head, String[] values, ArrayList<Integer> preselected, int[] image) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialogBuilder.setTitle(head);

        //code was creating conflict.Had to add nonsense value in final argument.Replace with correct one
        boolean arr[]=null;
        final MultiSelectAdapter adapter = new MultiSelectAdapter(context, values, image, arr);
        dialogBuilder.setAdapter(adapter, null);
        dialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.getselectedList();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", null);
        dialogBuilder.show();

    }

    public int getVal() {
        return val;
    }

    public int getAmount() {
        return amount;
    }

}
