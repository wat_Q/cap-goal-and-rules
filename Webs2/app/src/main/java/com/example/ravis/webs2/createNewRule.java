package com.example.ravis.webs2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

/**
 * Created by ravis on 6/20/2017.
 */

public class createNewRule extends AppCompatActivity {
    CardView myCardView;
    Toolbar t;
    RecyclerView myRecyclerView;
    RecyclerView.LayoutManager myLayoutManager;
    RecyclerView.Adapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createnewrule);
        t=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(t);
        if(getSupportActionBar()!=null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        DummyValues values=new DummyValues();
        myCardView=(CardView)findViewById(R.id.maincardview);
        myRecyclerView=(RecyclerView)findViewById(R.id.myRecyclerView);
        myRecyclerView.setNestedScrollingEnabled(false);
        myLayoutManager=new LinearLayoutManager(this);
        myAdapter=new cardAdapter(values,getApplicationContext());
        myRecyclerView.setLayoutManager(myLayoutManager);
        myRecyclerView.setAdapter(myAdapter);

    }
}
