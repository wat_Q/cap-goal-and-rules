package com.example.ravis.webs2.DatabaseHandle;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;


public class DatabaseCreator extends SQLiteOpenHelper{

    //database name
    public static final String DATABASE_NAME="capgoalrules.db";

    //table name
    public static final String TABLE_GOALS="Goals";
    public static final String TABLE_RULES="Rules";
    public static final String TABLE_MAPPING="mapping";

    //Goals column names
    public static final String GOAL_ID="goalid"; //mapping column name

    public static final String GOAL_NAME="goalname";
    public static final String GOAL_COLLECTED_AMOUNT="collectedamount";
    public static final String GOAL_TOTAL_AMOUNT="totalamount";
    public static final String GOAL_IMAGE="images";


    //Rules column names
    public static final String  RULE_ID="ruleid"; //mapping column name

    public static final String  RULE_NAME="rulename";
    public static final String  RULE_SAVE_AMOUNT="saveamount";
    public static final String  RULE_TRIGERRING_BANK="triggeringbank";
    public static final String  FUNDED_BANK="fundedbank";
    public static final String  RULE_MERCHANT="merchant";
    public static final String  RULE_DURATION="duration";
    public static final String  RULE_PERCENTAGE="savepercentage";
    public static final String  RULE_ORDER="weekorder";
    public static final String  RULE_SAVED_AMOUNT="savedamount";
    public static final String  RULE_IS_PAUSED="ispaused";
    public static final String  RULE_IMAGE="ruleimages";


    public static final String CREATE_GOAL_TABLE=
            "CREATE TABLE " +TABLE_GOALS+ "( "
            +GOAL_ID+               " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
            +GOAL_NAME+             " TEXT, "
            +GOAL_COLLECTED_AMOUNT+ " REAL, "
            +GOAL_TOTAL_AMOUNT+     " REAL, "
            +GOAL_IMAGE+            " INTEGER ); ";

    public static final String CREATE_RULE_TABLE=
            "CREATE TABLE " +TABLE_RULES+ "( "
            +RULE_ID+               " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
            +RULE_NAME+             " TEXT, "
            +RULE_SAVE_AMOUNT+      " INTEGER, "
            +RULE_TRIGERRING_BANK+  " TEXT, "
            +FUNDED_BANK+           " TEXT, "
            +RULE_MERCHANT+         " TEXT, "
            +RULE_DURATION+         " INTEGER, "
            +RULE_PERCENTAGE+       " INTEGER, "
            +RULE_ORDER+            " INTEGER, "
            +RULE_SAVED_AMOUNT+     " INTEGER, "
            +RULE_IS_PAUSED+        " INTEGER, "
            +RULE_IMAGE+            " INTEGER  ); ";



    public static final String CREATE_MAPPING_TABLE=
            "CREATE TABLE " +TABLE_MAPPING+ "("
            +GOAL_ID+               " INTEGER, "
            +RULE_ID+               " INTEGER, "
            +"PRIMARY KEY ("+GOAL_ID+","+RULE_ID+"), "
            +"FOREIGN KEY ("+GOAL_ID+") REFERENCES " +TABLE_GOALS+"("+GOAL_ID+") ON DELETE CASCADE, "
            +"FOREIGN KEY ("+RULE_ID+") REFERENCES " +TABLE_RULES+"("+RULE_ID+") ON DELETE CASCADE ); ";

    public static final String CREATE_MAPPING_TABLE2=
            "CREATE TABLE " +TABLE_MAPPING+ "("
                    +GOAL_ID+               " INTEGER REFERENCES " +TABLE_GOALS+"("+GOAL_ID+") ON DELETE CASCADE, "
                    +RULE_ID+               " INTEGER REFERENCES " +TABLE_RULES+"("+RULE_ID+") ON DELETE CASCADE, "
                    +"PRIMARY KEY ("+GOAL_ID+","+RULE_ID+")); ";


    public DatabaseCreator(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {

        super.onConfigure(db);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        db.setForeignKeyConstraintsEnabled(true);
        }else{ db.execSQL("PRAGMA foreign_keys=ON;");}
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_GOAL_TABLE);
        db.execSQL(CREATE_RULE_TABLE);
        db.execSQL("PRAGMA foreign_keys=ON;");
        db.execSQL(CREATE_MAPPING_TABLE2);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_GOALS);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_RULES);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_MAPPING);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON;");
        super.onOpen(db);
    }
}
