package com.example.ravis.webs2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class Create_edit_goals extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_goals);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarforcreateeditgoal);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.aleftarrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        EditText rules=(EditText)findViewById(R.id.rules);
        rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectrule=new Intent(Create_edit_goals.this,select_rule_act.class);
                startActivity(selectrule);
            }
        });
    }
}
