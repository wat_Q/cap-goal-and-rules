package com.example.ravis.webs2;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class MultiSelectAdapter extends ArrayAdapter {
    private int[] imageresource;
    private static boolean[] isselected;
    private boolean ischeckboxneeded;

    public MultiSelectAdapter(Context context, String[] item, int[] imageresource) {
        super(context, 0, item);
        this.imageresource = imageresource;
        ischeckboxneeded = false;
    }

    public MultiSelectAdapter(Context context, String[] item, int[] imageresource, boolean[] isselected) {
        super(context, 0, item);
        this.imageresource = imageresource;
        this.isselected = isselected;
        ischeckboxneeded = true;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_multiselectview, parent, false);
        }

        TextView name = (TextView) convertView.findViewById(R.id.textaccordingtorule);
        ImageView icon = (ImageView) convertView.findViewById(R.id.setmultiimage);
        name.setText(String.valueOf(getItem(position)));
        icon.setImageResource(imageresource[position]);
        final CheckBox select = (CheckBox) convertView.findViewById(R.id.checkBoxtobe);

        if (ischeckboxneeded) {
            select.setChecked(isselected[position]);
            select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isselected[position] = select.isChecked();
                }
            });
        } else {
            select.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }


    public void getselectedList(){
        Intent intent = new Intent("response");
        intent.putExtra("isselected",isselected);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }
}