package com.example.ravis.webs2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by aasthu on 14-Jun-17.
 */

public class spendlessrule extends AppCompatActivity {
    static Boolean firstclick_morethan = true, firstclick_save = true;
    LinearLayout spendlesslayout,perweeklayout,pickyourplace,using,yourss,towardyourbamk;
    TextView spendlessthantext;
    Context context;
    Dialog builder;
    TextView dialog_title, dialogmessage;
    EditText dialogedittext;
    ListView lv;
    Button okbutton, cancelbutton,spendlessrulebutton;

    int set=2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spend_lessrule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_ofspendless_rule);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.aleftarrow));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        spendlesslayout=(LinearLayout)findViewById(R.id.spendlesslayout);
        spendlessthantext=(TextView)findViewById(R.id.spendlessthan);
        context=this;
spendlessrulebutton=(Button)findViewById(R.id.buttonspendlessrule);
        spendlessrulebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(spendlessrule.this,already_created_goals.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.putExtra("position",helperfunctions.helperposition);
                finish();
                startActivity(in);
            }
        });
        final helperfunctions helperobject=new helperfunctions();
        final LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.customdialog_for_checkbox, null);
        builder = new Dialog(context);
        builder.setContentView(view);
        okbutton = (Button) builder.findViewById(R.id.custom_ok);
        cancelbutton = (Button) builder.findViewById(R.id.custom_cancle);
        dialog_title = (TextView) builder.findViewById(R.id.custom_dialog_title);
        dialogmessage = (TextView) builder.findViewById(R.id.custom_dialog_message);
         lv=(ListView)builder.findViewById(R.id.custom_listView);
        dialogedittext = (EditText) builder.findViewById(R.id.dialog_edit_text);
        dialogmessage.setTextColor(Color.parseColor("#1FA8CE"));
        dialogedittext.setTextColor(Color.parseColor("#000000"));
        okbutton.setTextColor(Color.parseColor("#b8d7e0"));
        dialogedittext.setHintTextColor(Color.parseColor("#808080"));
        spendlesslayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (firstclick_morethan) {
                    dialogedittext.setHint("30%");
                    firstclick_save = false;
                } else
                    dialogedittext.setHint(spendlessthantext.getText());
                okbutton.setEnabled(false);
                dialogmessage.setTextColor(Color.parseColor("#1FA8CE"));
                dialogedittext.setTextColor(Color.parseColor("#000000"));
                okbutton.setTextColor(Color.parseColor("#b8d7e0"));
                dialogedittext.setKeyListener(DigitsKeyListener.getInstance(false, false));

                dialogedittext.setVisibility(View.VISIBLE);
                helperobject.inputlimiter(dialogedittext,4);


                dialog_title.setText("Spend less than");
                dialogmessage.setText("Amount(max $5,000)");
                builder.show();

                //validation so that user could enter only less than or equal to 5000
                dialogedittext.addTextChangedListener(new TextWatcher() {

                    int savedval, curval;
                    String beforestring, afterstring;

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        beforestring = dialogedittext.getText().toString();
                        if (!((beforestring.equals("")))) {
                            savedval = Integer.parseInt(beforestring);
                        }
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                        afterstring = dialogedittext.getText().toString();
                        if (!afterstring.equals("")) {
                            okbutton.setTextColor(Color.parseColor("#1FA8CE"));
                            curval = Integer.parseInt(afterstring);
                            if (!(curval <= 5000)) {
                                dialogedittext.setText(Integer.toString(savedval));
                                dialogedittext.setSelection(3);
                            }
                            okbutton.setEnabled(true);
                        }
                        else
                        {
                            okbutton.setEnabled(false);
                            okbutton.setTextColor(Color.parseColor("#b8d7e0"));
                        }
                    }
                });
                cancelbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();
                        dialogedittext.setText("");
                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        spendlessthantext.setText("$"+dialogedittext.getText().toString());
                        dialogedittext.setText("");
                        builder.cancel();
                    }
                });
            }
        });


        perweeklayout=(LinearLayout)findViewById(R.id.perweeklayout);
        final TextView week=(TextView)findViewById(R.id.week);
        final String[] val={"week","month"};
        perweeklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               classforsingleandmultiselect d=new classforsingleandmultiselect(spendlessrule.this,week,val,"Select Interval");
                d.singlechoiceselection();
                /*Toast.makeText(spendlessrule.this,"hello",Toast.LENGTH_LONG).show();
                dialog_title.setText("Select Order");
                dialogedittext.setVisibility(View.INVISIBLE);
                dialogmessage.setVisibility(View.INVISIBLE);
                final CustomListViewforrounduprule cc=new CustomListViewforrounduprule(getApplicationContext(), val);
                lv.setAdapter(cc);
                cancelbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();
                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String a=  cc.getSelectedItem();
                        week.setText(a);
                        builder.cancel();
                    }
                });
                builder.show();*/
               /* alert.SingleSelectDialog("Save",set,val,new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int x=alert.getAmount();
                        set=alert.getAmount()-1;
                        week.setText("₹"+String.valueOf(x));

                    }
                });
*/
            }

        });

        pickyourplace=(LinearLayout)findViewById(R.id.atpalcelayout);
        pickyourplace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(spendlessrule.this,pickyourplace.class);
                startActivity(i);
            }
        });



        using=(LinearLayout)findViewById(R.id.using);
        using.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(spendlessrule.this,R.style.MyDialogTheme);
                alertDialogBuilder.setMessage("This rule is triggered by activity from these accounts:")
                        .setTitle("Triggering account")
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        Toast.makeText(spendlessrule.this,"You clicked yes button",Toast.LENGTH_LONG).show();
                                    }
                                }).create();
                alertDialogBuilder.show();
            }
        });



       towardyourbamk=(LinearLayout)findViewById(R.id.towardsyourbank);
        towardyourbamk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog_title.setText("Select Goals");
               dialogmessage.setText("Which goal should this rule fund?");
                dialogedittext = (EditText) builder.findViewById(R.id.dialog_edit_text);
                dialogedittext.setVisibility(View.INVISIBLE);
                cancelbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();

                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();
                    }
                });
               builder.show();
            }
        });



         yourss=(LinearLayout)findViewById(R.id.yoursbank);
        yourss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog_title.setText("Funding Banks");
                dialogmessage.setText("This Rule is funded from this bank");
                dialogedittext.setVisibility(View.INVISIBLE);
                cancelbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();
                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.cancel();
                    }
                });
                builder.show();}
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
