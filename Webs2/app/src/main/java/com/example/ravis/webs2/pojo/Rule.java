package com.example.ravis.webs2.pojo;

import java.util.List;

public class Rule {

    private int id=0, amount=0, savedamount=0, duration=0,savepercent=0;
    private String rulename=null, place=null, fundingbank=null, tiggeringbank=null;
    private List<Integer> towardsgoalids=null;
    private boolean order;

    //null reference
    public Rule() {
    }

    //for roundup rule
    public Rule(int id, String rulename, String tiggeringbank, int amount, List<Integer> towardsgoalids, String fundingbank) {
        this.id = id;
        this.amount = amount;
        this.rulename = rulename;
        this.fundingbank = fundingbank;
        this.tiggeringbank = tiggeringbank;
        this.towardsgoalids = towardsgoalids;
    }
    
    //for spend less rule
    public Rule(int id, String rulename, int amount, int duration, String place, String tiggeringbank, List<Integer> towardsgoalids, String fundingbank) {
        this.id = id;
        this.amount = amount;
        this.duration = duration;
        this.rulename = rulename;
        this.place = place;
        this.fundingbank = fundingbank;
        this.tiggeringbank = tiggeringbank;
        this.towardsgoalids = towardsgoalids;
    }
    
    //for guilty pleasure rule
    public Rule(int id, String rulename, String place, String tiggeringbank, int amount, List<Integer> towardsgoalids, String fundingbank) {
        this.id = id;
        this.amount = amount;
        this.rulename = rulename;
        this.place = place;
        this.fundingbank = fundingbank;
        this.tiggeringbank = tiggeringbank;
        this.towardsgoalids = towardsgoalids;
    }

    //for set & forget rule
    public Rule(int id, String rulename, int amount, int duration, List<Integer> towardsgoalids, String fundingbank) {
        this.id = id;
        this.amount = amount;
        this.duration = duration;
        this.rulename = rulename;
        this.fundingbank = fundingbank;
        this.towardsgoalids = towardsgoalids;
    }

    //for 52 week rule
    public Rule(int id, String rulename, int duration, boolean order, List<Integer> towardsgoalids, String fundingbank) {
        this.id = id;
        this.duration = duration;
        this.rulename = rulename;
        this.fundingbank = fundingbank;
        this.towardsgoalids = towardsgoalids;
        this.order = order;
    }

    //for freelancer rule
    public Rule(int id, String rulename, String tiggeringbank, int amount, int savepercent, List<Integer> towardsgoalids) {
        this.id = id;
        this.amount = amount;
        this.savedamount = 0;
        this.duration = 0;
        this.savepercent = savepercent;
        this.rulename = rulename;
        this.tiggeringbank = tiggeringbank;
        this.towardsgoalids = towardsgoalids;
    }


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSavedamount() {
        return savedamount;
    }
    public void setSavedamount(int savedamount) {
        this.savedamount = savedamount;
    }

    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getSavepercent() {
        return savepercent;
    }
    public void setSavepercent(int savepercent) {
        this.savepercent = savepercent;
    }

    public String getrulename() {
        return rulename;
    }
    public void setrulename(String rulename) {
        this.rulename = rulename;
    }

    public String getPlace() {
        return place;
    }
    public void setPlace(String place) {
        this.place = place;
    }
    
    public String getFundingbank() {
        return fundingbank;
    }
    public void setFundingbank(String fundingbank) {
        this.fundingbank = fundingbank;
    }

    public String getTiggeringbank() {
        return tiggeringbank;
    }
    public void setTiggeringbank(String tiggeringbank) {
        this.tiggeringbank = tiggeringbank;
    }

    public List<Integer> gettowardsgoalids() {
        return towardsgoalids;
    }
    public void settowardsgoalids(List<Integer> towardsgoalids) {
        this.towardsgoalids = towardsgoalids;
    }

    public boolean isOrder() {
        return order;
    }
    public void setOrder(boolean order) {
        this.order = order;
    }


}
