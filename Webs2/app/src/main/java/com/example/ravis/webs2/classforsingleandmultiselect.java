package com.example.ravis.webs2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

/**
 * Created by aasthu on 21-Jun-17.
 */

public class classforsingleandmultiselect {
    Context context;
    TextView editText;
    String selected;
    int[] images;
    boolean[] aa={false,true,false};
    String[] a;
    String title;
    AlertDialog.Builder builder;
    Dialog dialog;
    List<String> colorsList;
    public classforsingleandmultiselect(Context context,TextView editText ,String a[],String title) {
        builder=new AlertDialog.Builder(context,R.style.MyDialogTheme);
        dialog=new Dialog(context);
        this.context = context;
        this.editText=editText;
        this.title=title;
        this.a=a;
    }

    public classforsingleandmultiselect(Context context,TextView editText ,String a[],int[] images) {
        builder=new AlertDialog.Builder(context,R.style.MyDialogTheme);
        this.context = context;
        this.editText=editText;
        this.images=images;
        this.a=a;
    }


    void singlechoiceselection(){
        builder.setTitle(title);
        builder.setSingleChoiceItems(a,0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setPositiveButton("ok",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ListView lw = ((AlertDialog)dialog).getListView();
                Object checkedItem = lw.getAdapter().getItem(lw.getCheckedItemPosition());
                selected=(checkedItem.toString());
                editText.setText(selected);
               // Toast.makeText(context, "Success"+selected, Toast.LENGTH_SHORT).show();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).create();
        builder.show();
    }

    void multiselectionchoice()
    {
        //ListAdapter adapter=new ArrayAdapterWithIcon(context,a,images);
        builder.setTitle("Hello World");
        builder.setMultiChoiceItems(a, aa, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                aa[which]=isChecked;

            }
        });
        builder.setPositiveButton("ok",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                colorsList = Arrays.asList(a);
                editText.setText("Your preferred colors..... \n");
                String colors = "";
                for (int i = 0; i < aa.length; i++) {
                    boolean checked = aa[i];

                    if (checked) {
                        colors = colors + colorsList.get(i) ;
                        if (i != aa.length - 1) {
                            colors = colors + ", ";
                        }
                    }
                }
                editText.setText(editText.getText() + colors);
                dialog.cancel();


            }}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        }).create();
        builder.show();

    }




}


